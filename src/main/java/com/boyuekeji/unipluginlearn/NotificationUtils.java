package com.boyuekeji.unipluginlearn;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationChannelCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.Person;
import androidx.core.graphics.drawable.IconCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.GenericArrayType;

import io.dcloud.feature.uniapp.bridge.UniJSCallback;

/**
 * 通知的优先级，7.0版本及以前，直接在通知里设置，8.0及以后在通知渠道里设置
 *  在8.0及以上，使用渠道的importance来设置通知等级，以下则使用通知的priority来设置通知等级
 *  通知等级详情如下：
 *      urgent：有提示音没，显示通知
 *      high：有提示音
 *      medium：无提示音
 *      low：无提示音，不在状态栏显示
 * Android8.0版本开始，支持通知渠道，并且必须为通知分配渠道，否则通知不能显示
 * Android5.0版本开始，可以在锁屏上显示通知
 * Android7.0开始，可以直接在通知中进行回复或输入文字
 * Android10开始，平台可以自动生成操作按钮
 * Android7.0开始，可以使用通知组，对通知进行分类，如果同一应用发出4条及以上通知，则系统会自动将这些通知设置为一组
 * Android5.0开始，可以开启勿扰模式，所有通知的提示音和震动都会被关闭，勿扰模式分为3个等级
 *  完全静音：关闭全部提示音和震动
 *  仅限闹钟：除闹钟外的全部提示音和震动
 *  仅限优先事项：用户可以配置需要关闭声音和震动的通知
 * Android8.1开始，如果一秒内发出多条通知，只会有一个提示音，但是通知都会显示
 * NotificationCompat和NotificationManagerCompat是可以兼容低版本的，使用这个可以免去在代码中检查系统等级
 * 根据API版本不同，通知的一些变化
 *  Android4.1,API16
 *      推出展开式通知
 *      允许给通知添加按钮
 *      允许在设置中关闭通知
 *  Android4.4,API19和20
 *      添加了通知监听器服务
 *      API20添加了Android wear支持
 *  Android5.0,API21
 *      推出锁屏通知、提醒式通知
 *      推出勿扰模式
 *      添加通知等级功能，setPriority方法
 *      Android wear添加通知堆栈支持，setGroup方法将通知放入堆栈，手机和平板不支持堆栈
 *  Android7.0,API24
 *      重新设置通知木板样式，用来强调头像和图片
 *      添加3个通知模板：一个短信模板，另外两个借助展开式x选项和其他系统装饰来修饰自定义的内容视图
 *      手机和平板也支持Android5.0支持的Android wear的通知组
 *      可以在通知里进行回复
 *  Android8.0,API26
 *      必须将通知放入渠道
 *      可以按渠道来关闭通知，不用直接关闭应用的全部通知
 *      有效的通知会在图标上面显示通知的标志（如圆点）
 *      可以在通知栏暂停某个通知，设置通知超时时间
 *      可以设置通知的背景颜色
 *      部分通知相关的api迁移到了NotificationChannel
 */
public class NotificationUtils {

    public static final String UNI_NOTIFICATION_SMALL_ICON = "uniNotificationSmallIcon.png";
    public static String UNI_MEDIA_BROAD = "";
    public UniJSCallback uniJSCallback;

    @SuppressLint("RestrictedApi")
    public String notification(Context ctx, NotificationParam notificationParam, UniJSCallback jSCallback) {
        uniJSCallback = jSCallback;
        UNI_MEDIA_BROAD = ctx.getPackageName() + ".uniMedia";

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(ctx);
        if (notificationManagerCompat.getNotificationChannelCompat(notificationParam.channelId) == null) {
            Log.i(Utils.LOG_TAG, "没有指定渠道，创建");
            // Android8.0需要通知渠道。才能发布通知
            NotificationChannelCompat.Builder channelBuilder = new NotificationChannelCompat.Builder(notificationParam.channelId, notificationParam.channelPriority);
            // 可以禁止显示某一个渠道的图标圆点
            channelBuilder.setShowBadge(true);
            channelBuilder.setName(notificationParam.channelName);
            channelBuilder.setDescription(notificationParam.channelDesc);
            // 设置了震动后，会自动设置enable会true因此不用调用setVibrationEnabled
            channelBuilder.setVibrationEnabled(notificationParam.isVibration);
            // 下面的数组，连续两个为等待时间和震动时间，如下面的这个是等待0秒，震动0.5秒，等待0.3秒，再震动0.5秒
            if (notificationParam.vibrationPatternArr != null) {
                channelBuilder.setVibrationPattern(notificationParam.vibrationPatternArr);
            }

            if (notificationParam.lightColorArr != null) {
                channelBuilder.setLightsEnabled(true);
                channelBuilder.setLightColor(Color.argb(notificationParam.lightColorArr[0], notificationParam.lightColorArr[1], notificationParam.lightColorArr[2], notificationParam.lightColorArr[3]));
            }

            if (notificationParam.sound != null) {
                channelBuilder.setSound(Uri.fromFile(new File(notificationParam.sound)), null);
            }
            notificationManagerCompat.createNotificationChannel(channelBuilder.build());
        }

        NotificationCompat.Builder notificationCompatBuilder = new NotificationCompat.Builder(ctx, notificationParam.channelId)
                // 通知标题
                .setContentTitle(notificationParam.title)
                // 通知内容
                .setContentText(notificationParam.text);
        notificationCompatBuilder.setPriority(notificationParam.priority);

        if (notificationParam.smallIcon == null) {
            String uniIconPath = ctx.getCacheDir() + "/" + UNI_NOTIFICATION_SMALL_ICON;
            if (!new File(uniIconPath).exists()) {
                cpNotificationSmallIcon(ctx);
            }
            notificationParam.smallIcon = ctx.getCacheDir() + "/" + UNI_NOTIFICATION_SMALL_ICON;

        }
        if (!new File(notificationParam.smallIcon).exists()) {
            return "smallIcon文件不存在: " + notificationParam.smallIcon;
        }
        notificationCompatBuilder.setSmallIcon(IconCompat.createWithBitmap(BitmapFactory.decodeFile(notificationParam.smallIcon)));

        if (notificationParam.largeIcon != null) {
            if (!new File(notificationParam.largeIcon).exists()) {
                return "largeIcon文件不存在: " + notificationParam.largeIcon;
            }
            notificationCompatBuilder.setLargeIcon(BitmapFactory.decodeFile(notificationParam.largeIcon));
        }

        // 可选，通知里的大文本，主要用来显示大量内容
        if (notificationParam.bigText != null) {
            notificationCompatBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(notificationParam.bigText));
        }

        if (notificationParam.bigPicture != null) {
            if (!new File(notificationParam.bigPicture).exists()) {
                return "bigPicture文件不存在: " + notificationParam.bigPicture;
            }
            notificationCompatBuilder.setStyle(new NotificationCompat.BigPictureStyle()
            .bigPicture(BitmapFactory.decodeFile(notificationParam.bigPicture))
            .bigLargeIcon(null));
        }

        // 可选，显示角标的数字
        if (notificationParam.badgeNumber != null) {
            notificationCompatBuilder.setNumber(notificationParam.badgeNumber);
        }

        if (notificationParam.sound != null) {
            if (!new File(notificationParam.sound).exists()) {
                return "sound文件不存在: " + notificationParam.sound;
            }
            notificationCompatBuilder.setSound(Uri.fromFile(new File(notificationParam.sound)));
        }

        // 设置跳转到主界面
        PackageManager packageManager = ctx.getPackageManager();
        Intent it = packageManager.getLaunchIntentForPackage(ctx.getPackageName());
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0, it, 0);
        notificationCompatBuilder.setContentIntent(pendingIntent);

        // 可选，点击通知跳到指定activity
        // 下面的代码会自动调用setAutoCancel方法，点击通知，跳到指定activity后，通知会自动消失
        if (notificationParam.toActivityName != null) {
            try {
                it = new Intent(ctx, Class.forName(notificationParam.toActivityName));
                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                pendingIntent = PendingIntent.getActivity(ctx, 0, it, 0);
                notificationCompatBuilder.setContentIntent(pendingIntent);
            } catch (ClassNotFoundException e) {
                Log.e(Utils.LOG_TAG, "没有指定的activity name，请检查");
            }
        }

        // 可选，给通知添加按钮，待研究
        // 可选，添加快速回复，待研究

        // 可选，添加进度条，更新进度条时，请使用相同的notificationId
        if (notificationParam.progressMax != null) {
            Log.i(Utils.LOG_TAG, notificationParam.progressMax + ", " + notificationParam.progressCurrent + ", " + notificationParam.progressAnimation);
            notificationCompatBuilder.setProgress(notificationParam.progressMax, notificationParam.progressCurrent, notificationParam.progressAnimation);
        }

        if (notificationParam.isShowWhen != null) {
            notificationCompatBuilder.setShowWhen(notificationParam.isShowWhen);
        }

        if (notificationParam.when != null) {
            // 设置通知的时间，通知面板会根据这个时间来排序，毫秒
            notificationCompatBuilder.setWhen(notificationParam.when);
        }

        if (notificationParam.timeoutAfter != null) {
            notificationCompatBuilder.setTimeoutAfter(notificationParam.timeoutAfter);
        }

        if (notificationParam.category != null) {
            notificationCompatBuilder.setCategory(notificationParam.category);
        }

        if (notificationParam.smallIconColorArr != null) {
            notificationCompatBuilder.setColorized(true);
            notificationCompatBuilder.setColor(Color.argb(notificationParam.smallIconColorArr[0], notificationParam.smallIconColorArr[1], notificationParam.smallIconColorArr[2], notificationParam.smallIconColorArr[3]));
        }

        if (notificationParam.inboxArr != null) {
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            for (String info: notificationParam.inboxArr) {
                inboxStyle.addLine(info);
            }
            notificationCompatBuilder.setStyle(inboxStyle);
        }

        // 下面的代码跟官方的示例展示出来的不同，待研究原因
        /*Person.Builder personBuilder = new Person.Builder();
        personBuilder.setBot(false);
        personBuilder.setImportant(false);
        personBuilder.setIcon(IconCompat.createWithBitmap(BitmapFactory.decodeFile(ctx.getCacheDir() + "/" + UNI_NOTIFICATION_SMALL_ICON)));
        personBuilder.setName("123");
        NotificationCompat.MessagingStyle.Message messaging = new NotificationCompat.MessagingStyle.Message("aaa", System.currentTimeMillis(), personBuilder.build());
        NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(personBuilder.build());
        messagingStyle.addMessage(messaging);
        notificationCompatBuilder.setStyle(messagingStyle);*/

        // 设置多媒体
        if (notificationParam.isMedia) {
            for (int i=0; i<notificationParam.btnArr.length; i++) {
                PendingIntent preIntent = PendingIntent.getBroadcast(ctx, i, new Intent().setAction(UNI_MEDIA_BROAD).putExtra("media", i), PendingIntent.FLAG_CANCEL_CURRENT);
                NotificationCompat.Action action= new NotificationCompat.Action.Builder(IconCompat.createWithBitmap(BitmapFactory.decodeFile(notificationParam.btnArr[i][0])), notificationParam.btnArr[i][0], preIntent).build();
                notificationCompatBuilder.addAction(action);
            }

            notificationCompatBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            notificationCompatBuilder.setStyle(new androidx.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0, 1, 2));

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(UNI_MEDIA_BROAD);
            MediaReceiver mediaReceiver = new MediaReceiver();
            ctx.registerReceiver(mediaReceiver, intentFilter);
        }

        // 显示通知
        // 这个id代表的是单个通知，更新或移除这个通知时，需要这个id
        int notificationId = notificationParam.notificationId != null ? notificationParam.notificationId : 1;
        notificationManagerCompat.notify(notificationId, notificationCompatBuilder.build());

        // 取消指定id的通知
        if (notificationParam.isCancel) {
            notificationManagerCompat.cancel(notificationId);
        }

        // 取消全部通知
        if (notificationParam.isCancelAll) {
            notificationManagerCompat.cancelAll();
        }

        return "success";
    }

    public class MediaReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(Utils.LOG_TAG, "接收到广播: " + intent.getIntExtra("media", 0));
            int btnIndex = intent.getIntExtra("media", 0);
            uniJSCallback.invokeAndKeepAlive(btnIndex);
            context.unregisterReceiver(this);
        }
    }

    public void cpNotificationSmallIcon(Context ctx) {
        try {
            InputStream is = ctx.getAssets().open("img/" + UNI_NOTIFICATION_SMALL_ICON);
            FileOutputStream fos = new FileOutputStream(ctx.getCacheDir() + "/" + UNI_NOTIFICATION_SMALL_ICON);
            byte[] bArr = new byte[is.available()];
            is.read(bArr, 0, bArr.length);
            fos.write(bArr,0, bArr.length);
            fos.flush();
            fos.close();
            is.close();
        } catch (Exception e) {
            Log.e(Utils.LOG_TAG, "复制小图标失败: " + e.getMessage());
        }
    }

}
