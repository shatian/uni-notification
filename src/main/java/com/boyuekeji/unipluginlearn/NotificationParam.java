package com.boyuekeji.unipluginlearn;

import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.security.PublicKey;

/**
 * 创建Notification的参数
 */

public class NotificationParam {
    public String title;
    public String text;
    public String bigText;
    public String bigPicture;
    // 通知的级别 -2 - 2
    public Integer priority = NotificationCompat.PRIORITY_HIGH;
    // 渠道的级别 0-5
    public Integer channelPriority = NotificationManagerCompat.IMPORTANCE_HIGH;
    public String channelId = "uniNotification1";
    public String channelName = "uniNotification";
    public String channelDesc = "Android端的Notification的Uni插件";
    public String toActivityName;
    public Integer badgeNumber;
    public Integer notificationId;

    public Integer progressMax;
    public Integer progressCurrent;
    public Boolean progressAnimation = false;
    public Boolean isShowWhen;
    public Long when;
    public Long timeoutAfter;
    public String smallIcon;
    public String largeIcon;
    public String sound;

    public Boolean isVibration = false;
    public long[] vibrationPatternArr;
    public String category;
    public String group;
    public int[] smallIconColorArr;
    public int[] lightColorArr;
    public String[] inboxArr;

    public boolean isCancel;
    public boolean isCancelAll;

    public boolean isMedia;
    public String[][] btnArr;

}
